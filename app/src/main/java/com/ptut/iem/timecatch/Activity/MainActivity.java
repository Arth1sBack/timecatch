package com.ptut.iem.timecatch.Activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.parse.ui.ParseLoginActivity;
import com.parse.ui.ParseLoginBuilder;
import com.ptut.iem.timecatch.Datas.Etude;
import com.ptut.iem.timecatch.Datas.Reponse;
import com.ptut.iem.timecatch.Notification.ScheduledNotificationService;
import com.ptut.iem.timecatch.R;
import com.ptut.iem.timecatch.Singleton.EtudesManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends Activity{
    private final String user_id = "com.example.app.user_id";
    private final String user_mail = "com.example.app.user_mail";
    private final String user_heureDeb = "com.example.app.user_heureDeb";
    private final String user_heureFin = "com.example.app.user_heureFin";

    private SharedPreferences prefs;
    private boolean firstConnection;
    private Reponse currentReponse;

    private int mNotifIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Création des shared preferences
        prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);

        currentReponse = EtudesManager.getInstance().getLastReponse();

        onNewIntent(getIntent());

        openLoginPage();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openLoginPage(){
        // open login page

        ParseLoginBuilder builder = new ParseLoginBuilder(MainActivity.this);
        startActivityForResult(builder.build(), 0);
        /*
        Intent k = new Intent(this, MoodPairsActivity.class);
        startActivityForResult(k,0);*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(resultCode){
            case ParseLoginActivity.RESULT_OK://-1
                // conexion ok
                parseParamsBecomeUserPref();
                shouldLaunchTraining();
                displayCG();
                break;

            case MoodPairsActivity.RESULT_OK://99
                // de retour de l'écran des moodpairs
                currentReponse.setMoods(data.getParcelableArrayExtra("moodspairs"));
                currentReponse.setDate();
                displayExplanationsBeforeTest();
                break;

            case TestActivity.RESULT_OK://100
                // de retour de l'écran des tests
                currentReponse.setEstimations(data.getParcelableArrayExtra("estimations"));
                endTest();
                break;

            case OptionsActivity.RESULT_OK://101
                // de retour de l'écran des options
                String mail = data.getStringExtra("userpref_mail");
                Date heureDeb = new Date(data.getLongExtra("userpref_heureDeb",0));
                Date heureFin = new Date(data.getLongExtra("userpref_heureFin",0));
                saveUserPref(mail, heureDeb, heureFin);
                if (firstConnection){
                    displayThanksForRegistration();
                }else{
                    finish();
                    System.exit(0);
                }
                break;
        }
    }

    public void shouldLaunchTraining(){
        // Lecture des prefs utilisateurs a la recherche de l'id
        String id = prefs.getString(user_id, "");
        // Recherche de l'id de l'user sur Parse
        String idParse = ParseUser.getCurrentUser().getObjectId();

        firstConnection = (id==""||!id.equals(idParse));
        prefs.edit().putString(user_id,idParse).apply();
        currentReponse.setId_user(prefs.getString(user_id, ""));
    }

    public void getMoodPairs(){
        // Intent vers MoodPairsActivity
        try
        {
            Intent k = new Intent(this, MoodPairsActivity.class);
            startActivityForResult(k,0);
        }catch(Exception e){

        }
    }

    public void launchTest(){
        // lancer les tests
        Etude etude;
        Date aujd = new Date();

        if(firstConnection){
            etude = new Etude(1000,2200,8,"",0, aujd, aujd );
        }else{
            etude = EtudesManager.getInstance().getEtude();
        }

        // Intent vers TestActivity
        try
        {
            Intent k = new Intent(this, TestActivity.class);
            k.putExtra("etude",etude);
            startActivityForResult(k,0);
        }catch(Exception e){

        }
    }

    private void displayCG(){
        // afficher l'alert avec les CG
        String text = getResources().getString(R.string.CGText,EtudesManager.getInstance().getEtude().getNbNotif());
        new AlertDialog.Builder(this)
                .setTitle(R.string.CGTitle)
                .setMessage(text)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //OK
                        // afficher le choix pour l'heure
                        openOptionActivity();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // NON
                        // user decline term and conditions
                        //delete user
                        finish();
                        System.exit(0);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void endTest(){
        currentReponse.setId_user(prefs.getString(user_id, ""));

        if(firstConnection){
            String text = getResources().getString(R.string.EndTrainingText,EtudesManager.getInstance().getEtude().getNbNotif());
            SimpleDateFormat timeFormatter = new SimpleDateFormat("dd/mm/YYYY");
            String dateFin = timeFormatter.format(EtudesManager.getInstance().getEtude().getDateFin());

            new AlertDialog.Builder(this)
                    .setTitle(R.string.EndTrainingTitle)
                    .setMessage(text + " " + dateFin)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //OK
                            // kill app
                            finish();
                            System.exit(0);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }else{
            // envoie les rep
            // Si pb à l'envoie des rep : le délai entre la commande d'envoi et le kill de l'app est trop court
            EtudesManager.getInstance().sendThisShit(currentReponse);
            finish();
            System.exit(0);
        }
    }

    private void openOptionActivity(){
        //Get User pref
        String mail = prefs.getString(user_mail, "");
        long heureDeb = prefs.getLong(user_heureDeb, 0);
        long heureFin = prefs.getLong(user_heureFin, 0);

        // Intent vers OptionsActivity
        try
        {
            Intent k = new Intent(this, OptionsActivity.class);
            k.putExtra("userpref_mail",mail);
            k.putExtra("userpref_heureDeb",heureDeb);
            k.putExtra("userpref_heureFin",heureFin);

            startActivityForResult(k, 0);

            //finish()
        }catch(Exception e){

        }
    }

    private void saveUserPref(String mail, Date heureDeb, Date heureFin){
        prefs.edit().putString(user_mail, mail).apply();
        prefs.edit().putLong(user_heureDeb, heureDeb.getTime()).apply();
        prefs.edit().putLong(user_heureFin, heureFin.getTime()).apply();

        ParseUser user = ParseUser.getCurrentUser();

        user.setEmail(mail);
        user.put("heure_debut",heureDeb);
        user.put("heure_fin",heureFin);
        user.saveEventually();
    }

    public void parseParamsBecomeUserPref(){
        //Récupération des infos dans Parse
        ParseUser user = ParseUser.getCurrentUser();
        //String id = user.getCurrentUser().getObjectId();
        String mail = user.getCurrentUser().getEmail();
        Date heureDeb = user.getDate("heure_debut");
        Date heureFin = user.getDate("heure_fin");

        //Ecriture de l'id dans les prefs utilisateurs
        //prefs.edit().putString(user_id, id).apply();
        prefs.edit().putString(user_mail, mail).apply();
        if(heureDeb != null && heureFin != null){
            prefs.edit().putLong(user_heureDeb,heureDeb.getTime()).apply();
            prefs.edit().putLong(user_heureFin,heureFin.getTime()).apply();
        }

    }

    @Override
    protected void onNewIntent (Intent intent) {
        Bundle mBundle = intent.getExtras();
        //sentValueIndex = mBundle.getString("notifIndex");
        try{
            mNotifIndex = mBundle.getInt("notifIndex", -1);
            if (mNotifIndex != -1) {
                Log.d("indexNotifActivityReceiver", String.valueOf(mNotifIndex));
                //mNotifIndex = Integer.parseInt(sentValueIndex);
                //Log.d("indexNotifActivityReceiver", String.valueOf(mNotifIndex));
                Intent nextIntent = new Intent(this, ScheduledNotificationService.class);
                nextIntent.putExtra("notifIndex", mNotifIndex);
                if (mNotifIndex > 0) {
                    startService(nextIntent);
                } else {
                    stopService(nextIntent);
                    Log.d("MyServiceReceiver", "Service stop");
                }
                NotificationManager notificationManager = (NotificationManager) this
                        .getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(0);
            }
        }catch(NullPointerException e){

        }
    }

    public void displayThanksForRegistration(){
        // Doit-on afficher les CG + l'entrainement ?
        String text = getResources().getString(R.string.EndCreationProfilText,EtudesManager.getInstance().getEtude().getN_rep());
        new AlertDialog.Builder(this)
                .setTitle(R.string.EndCreationProfilTitle)
                .setMessage(text)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //OK
                        // affiche les moodPairs
                        getMoodPairs();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    public void displayExplanationsBeforeTest(){
        new AlertDialog.Builder(this)

                .setTitle(R.string.BeforeTestTitle)
                .setMessage(R.string.BeforeTestText)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //OK
                        launchTest();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}

package com.ptut.iem.timecatch.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ptut.iem.timecatch.Adapters.MoodAdapter;
import com.ptut.iem.timecatch.Singleton.EtudesManager;
import com.ptut.iem.timecatch.Datas.Mood;
import com.ptut.iem.timecatch.Datas.Reponse;
import com.ptut.iem.timecatch.R;

import java.util.ArrayList;
import java.util.List;


public class MoodPairsActivity extends Activity {
    public static final int RESULT_OK = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mood_pairs);

        // génère autant de slider qu'il y a de moods
        setMoodsSliders();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mood_pairs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setMoodsSliders(){
        // obtenir la liste des moods
        List<Mood> moodsList;
        moodsList = EtudesManager.getInstance().getMoods();

        ListView listMoodPairs = (ListView)findViewById(R.id.listMoodPairs);

        // get data from the table by the ListAdapter
        MoodAdapter moodAdapter = new MoodAdapter(this, R.layout.cell_moodpair, moodsList);
        listMoodPairs.setAdapter(moodAdapter);
    }


    public void onNextButtonClick(View view) {
        ListView listMoodPairs = (ListView)findViewById(R.id.listMoodPairs);

        Mood results[] = new Mood[listMoodPairs.getChildCount()];

        for(int i = 0; i< listMoodPairs.getChildCount();i++){
            View v = listMoodPairs.getChildAt(i);

            TextView badmood = (TextView)v.findViewById(R.id.badmood);
            TextView goodmood = (TextView)v.findViewById(R.id.goodmood);
            SeekBar seekBar = (SeekBar)v.findViewById(R.id.seekBar);
            int feeling = seekBar.getProgress();

            Mood moodResult = new Mood(badmood.getText().toString(),goodmood.getText().toString(),feeling);

            results[i] = moodResult;
        }
        // retour a Main en passant result en paramètre
        Intent resultIntent = new Intent();
        resultIntent.putExtra("moodspairs", results);
        setResult(MoodPairsActivity.RESULT_OK, resultIntent);
        finish();
    }

    // désactivation du bouton retour
    @Override
    public void onBackPressed() {
    }
}

package com.ptut.iem.timecatch.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ptut.iem.timecatch.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class OptionsActivity extends Activity {

    public static final int RESULT_OK = 101;

    private TextView timeViewBegin, timeViewEnd;
    private int days, hours, min, second;
    private Calendar calendar;
    private EditText editTextEmail;
    private Button btnValidate;
    private SimpleDateFormat dateFormat;

    private String mail;
    private Date heureDeb;
    private Date heureFin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        //get extra
        Intent i = getIntent();
        mail = i .getStringExtra("userpref_mail");
        heureDeb = new Date(i.getLongExtra("userpref_heureDeb",0));
        heureFin = new Date(i.getLongExtra("userpref_heureFin",0));

        calendar = Calendar.getInstance();
        timeViewBegin = (TextView) findViewById(R.id.timePickerBegin);
        timeViewEnd = (TextView) findViewById(R.id.timePickerEnd);
        hours = calendar.get(Calendar.HOUR);
        min = calendar.get(Calendar.MINUTE);
        second = calendar.get(Calendar.SECOND);
        dateFormat = new SimpleDateFormat("HH:mm"); // Set le format de la date

        editTextEmail = (EditText) findViewById(R.id.email);
        editTextEmail.setText(mail);

        String heureDebDisplayed = dateFormat.format(heureDeb);
        timeViewBegin.setText(heureDebDisplayed);

        String heureFinDisplayed = dateFormat.format(heureFin);
        timeViewEnd.setText(heureFinDisplayed);

        btnValidate = (Button) findViewById(R.id.validate);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Set l'heure dans les champ textView
    private void showTime(int hour, int minute, TextView timeView) {
        String min = String.valueOf(minute);
        String mHour = String.valueOf(hour);
        if (hour < 10) {
            mHour = "0" + mHour;
        }
        if (minute < 10) {
            min = "0" + min;
        }
        timeView.setText(mHour + ":" + min);
    }

    // Fonction onclick()
    @SuppressWarnings("deprecation")
    public void setTimeBegin(View view) {
        showDialog(888);
    }
    // Fonction onclick()
    @SuppressWarnings("deprecation")
    public void setTimeEnd(View view) {
        showDialog(889); // id du dialogue à créer
    }

    // Affiche les timePicker
    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        // Si l'id du dialogue est de x on retourne la value suivante
        if (id == 888) {
            return new TimePickerDialog(this, myTimeListenerBegin, hours, min, true);
        } else if (id == 889) {
            return new TimePickerDialog(this, myTimeListenerEnd, hours, min, true);
        }
        return null;
    }

    // Créer le timepicker pour l'heure de début
    private TimePickerDialog.OnTimeSetListener myTimeListenerBegin
            = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            showTime(hourOfDay, minute, timeViewBegin); // Fonction qui est appelé lors du clic sur "ok" après le choix de l'heure
        }
    };

    // Créer le timepicker pour l'heure de fin
    private TimePickerDialog.OnTimeSetListener myTimeListenerEnd
            = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            showTime(hourOfDay, minute, timeViewEnd); // Fonction qui est appelé lors du clic sur "ok" après le choix de l'heure
            String dateStringDeb= timeViewBegin.getText().toString(); // Récupère l'heure de début
            String dateStringFin= timeViewEnd.getText().toString(); // Récupère l'heure de début

            verification(dateStringDeb, dateStringFin);
        }
    };

    private boolean verification(String dateStringDeb, String dateStringFin) {
        Date convertedDateBegin, convertedDateEnd;
        try {
            convertedDateBegin = dateFormat.parse(dateStringDeb); // convertion de l'heure de début type string en type date
            convertedDateEnd = dateFormat.parse(dateStringFin); // convertion de l'heure de fin type string en type date
            // Calucule de la différence entre les deux heures
            long difference = convertedDateEnd.getTime() - convertedDateBegin.getTime();
            days = (int) (difference / (1000*60*60*24));
            hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
            min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
            hours = (hours < 0 ? -hours : hours); // différence des deux heures
            //Log.i("======= Hours", " :: " + hours);

            // si l'heure de fin est plus grande que l'heure de début ET qu'il y a une différence de 8h c'est ok
            if((convertedDateBegin.compareTo(convertedDateEnd) < 0) && hours >= 8){
                //Log.d("Compare Date", "Ok !");
                btnValidate.setClickable(true);
                return true;
            } else { // Sinon on affiche un toast pour expliquer ET on rend nonClickable le bouton de validation
                //Log.d("Compare Date", "Vous devez rentrer une plage de 8h minimum");
                btnValidate.setClickable(false);
                Toast.makeText(getApplicationContext(), "Vous devez rentrer une plage de 8h minimum", Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }


    public void onClick(View view) {
        try {
            String dateStringDeb = timeViewBegin.getText().toString(); // Récupère l'heure de début
            String dateStringFin = timeViewEnd.getText().toString(); // Récupère l'heure de début
            if(verification(dateStringDeb,dateStringFin)){
                String mail = editTextEmail.getText().toString();

                Date convertedDateBegin, convertedDateEnd;

                convertedDateBegin = dateFormat.parse(dateStringDeb); // convertion de l'heure de début type string en type date
                convertedDateEnd = dateFormat.parse(dateStringFin); // convertion de l'heure de fin type string en type date

                // retour a Main en passant les données en paramètre
                Intent resultIntent = new Intent();
                resultIntent.putExtra("userpref_mail", mail);
                resultIntent.putExtra("userpref_heureDeb",convertedDateBegin.getTime());
                resultIntent.putExtra("userpref_heureFin",convertedDateEnd.getTime());

                setResult(OptionsActivity.RESULT_OK, resultIntent);
                finish();
            }
        }catch(Exception e){
            Log.d("OptionsActivity",e.getMessage());
        }
    }

    // désactivation du bouton retour
    @Override
    public void onBackPressed() {
    }
}

package com.ptut.iem.timecatch.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptut.iem.timecatch.Datas.Estimation;
import com.ptut.iem.timecatch.Datas.Etude;
import com.ptut.iem.timecatch.R;
import com.ptut.iem.timecatch.Singleton.EtudesManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestActivity extends Activity {
    public static final int RESULT_OK = 100;
    public static final long BLACK_DELAY = 2000;    //délais de noir av/ap l'affichage du signal

    public enum TutoStyle {
        Short,
        Long;
    }

    private ImageView logo;
    private Button bouton1;
    private Button bouton2;
    private TextView title;

    private int tutorialTimes;  //nombre de fois que doit etre affcihé le tutoriel
    private int testNumber;
    private List<Estimation> estimations;    //stocke les réponses
    private Long durations[];

    private Etude etude;
    private TutoStyle tutoStyle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        Intent i = getIntent();
        Etude etude = i.getParcelableExtra("etude");
        this.etude = etude;
        this.tutorialTimes = 4; //2 longs + 2 courts
        this.tutoStyle = TutoStyle.Short;

        this.durations = getDurations();
        this.estimations = new ArrayList<>();
        this.testNumber = 0;
        this.logo = (ImageView)findViewById(R.id.imageView_logo);
        this.bouton1 = (Button)findViewById(R.id.button_durationOne);
        this.bouton2 = (Button)findViewById(R.id.button_durationTwo);
        this.title = (TextView)findViewById(R.id.text);

        logo.setVisibility(View.GONE);

        shouldLaunchTutorial();
        //newTest();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onAnswerClick(View view) {
        Object tag = view.getTag();
        int rep = Integer.parseInt((String) tag);

        long presentee = durations[testNumber-1];
        long reponse = 0;

        switch (rep){
            case 1:
                reponse = etude.getDuree_min();
                break;
            case 2:
                reponse = etude.getDuree_max();
                break;
        }

        Estimation estimation = new Estimation(presentee,reponse);
        estimations.add(estimation);

        if(this.estimations.size() < this.etude.getN_rep()){
            newTest();
        }else{
            testEnded();
        }
    }



    private void tutorialShortDuration(){
        title.setText(R.string.ThisIsShortDuration);
        // écran noir puis affichage du logo
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                displayLogoDuringTime(etude.getDuree_min());
            }
        }, BLACK_DELAY);
    }

    private void tutorialLongDuration(){
        title.setText(R.string.ThisIsLongDuration);
        // écran noir puis affichage du logo
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                displayLogoDuringTime(etude.getDuree_max());
            }
        }, BLACK_DELAY);
    }

    private void shouldLaunchTutorial(){
        // cacher les boutons
        bouton1.setVisibility(View.GONE);
        bouton2.setVisibility(View.GONE);

        if(this.tutorialTimes > 0){
            if(this.tutoStyle == TutoStyle.Short){
                this.tutoStyle = TutoStyle.Long;
                tutorialShortDuration();
            }else{
                this.tutoStyle = TutoStyle.Short;
                tutorialLongDuration();
            }
        }else{
            String text = getResources().getString(R.string.BeforeEstimationText,EtudesManager.getInstance().getEtude().getN_rep());
            new AlertDialog.Builder(this)
                    .setTitle(R.string.BeforeEstimationTitle)
                    .setMessage(text)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //OK
                            // afficher le choix pour l'heure
                            newTest();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }


    private void newTest(){
        //test
        this.testNumber++;

        // cacher les boutons
        bouton1.setVisibility(View.GONE);
        bouton2.setVisibility(View.GONE);

        // set le titre

        String text = getResources().getString(R.string.ThisIsDurationNumber);
        title.setText(text + testNumber);// + " - " + durations[testNumber-1]);

        // écran noir puis affichage du logo
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                displayLogoDuringTime(durations[testNumber - 1]);
            }
        }, BLACK_DELAY);
     }

    // Renvoie un tableau contenant les durées dans l'ordre ou elles seront affichées
    private Long[] getDurations(){
        Random rand = new Random();
        // définition des temps affichables
        List<Long> tempsAffichables = new ArrayList();

        float approxNbIntervale = (etude.getN_rep()/4);
        int nbIntervale = Math.round(approxNbIntervale);
        long intervale = getIntervale(etude.getDuree_max(), etude.getDuree_min());
        long pas = intervale/(nbIntervale-1);

        long tempsMin = (etude.getDuree_min()<etude.getDuree_max())?etude.getDuree_min():etude.getDuree_max();

        //créer une liste avec les durées souhaitées
        tempsAffichables.add(tempsMin);
        for(int i = 1;i<nbIntervale;i++){
            tempsAffichables.add(tempsMin + (i*pas));
        }

        //créer un tableau aves les temps mélangés
        Long durations[] = new Long[etude.getN_rep()];
        for(int j = 0; j<4;j++){
            List<Integer> randomUse = new ArrayList<>();
            for(int i=0; i<nbIntervale;i++){
                //tirer un nombre entre 0&5
                int randomNumber = rand.nextInt(tempsAffichables.size());
                //tant que ce nombre a déja été tiré on en prend un autre
                while (randomUse.contains(randomNumber)){
                    randomNumber = rand.nextInt(tempsAffichables.size());
                }
                //on ajoute a la suite du tableau la valeur correspondante
                durations[(j*nbIntervale)+i]=tempsAffichables.get(randomNumber);
                //on retire cette valeur des nombres tirables
                randomUse.add(randomNumber);
            }
            randomUse.clear();
        }
        return durations;
    }

    // Obtention de l'intervale
    private long getIntervale(long duree1, long duree2){
        long min, max;

        if (duree1 > duree2){
            max = duree1;
            min = duree2;
        }else{
            max = duree2;
            min = duree1;
        }
        return  max - min;
    }

    // affiche le logo
    private void displayLogoDuringTime(long time){
        logo.setVisibility(View.VISIBLE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                logo.setVisibility(View.GONE);
                displayButtonsWithDelay(BLACK_DELAY);
            }
        }, time);
    }

    // affichage des boutons après délai
    private void displayButtonsWithDelay(long time){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if(tutorialTimes>0) {
                    tutorialTimes--;
                    shouldLaunchTutorial();
                }else{
                    bouton1.setVisibility(View.VISIBLE);
                    bouton2.setVisibility(View.VISIBLE);
                }
            }
        }, time);
    }

    private void testEnded(){
        // copie de la liste dans un tableau
        Estimation results[] = new Estimation[estimations.size()];
        for(int i = 0; i< estimations.size();i++){
            results[i] = estimations.get(i);
        }

        // retour a Main en passant result en paramètre
        Intent resultIntent = new Intent();
        resultIntent.putExtra("estimations", results);
        setResult(TestActivity.RESULT_OK, resultIntent);
        finish();
    }

    // désactivation du bouton retour
    @Override
    public void onBackPressed() {
    }
}

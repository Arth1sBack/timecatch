package com.ptut.iem.timecatch.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ptut.iem.timecatch.Datas.Mood;
import com.ptut.iem.timecatch.R;

import java.util.List;

/**
 * Created by iem on 07/01/15.
 */
public class MoodAdapter extends ArrayAdapter<Mood> {

    public MoodAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public MoodAdapter(Context context, int resource, List<Mood> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {

            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.cell_moodpair, null);
        }

        Mood aMood = getItem(position);

        if (aMood != null) {

            TextView badmood = (TextView) v.findViewById(R.id.badmood);
            TextView goodmood = (TextView) v.findViewById(R.id.goodmood);

            badmood.setText(aMood.getBadMood());
            goodmood.setText(aMood.getGoodMood());
        }

        return v;

    }
}

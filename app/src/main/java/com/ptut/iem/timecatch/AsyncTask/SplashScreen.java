package com.ptut.iem.timecatch.AsyncTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.parse.Parse;
import com.ptut.iem.timecatch.Activity.MainActivity;
import com.ptut.iem.timecatch.Activity.SplashScreenActivity;
import com.ptut.iem.timecatch.Singleton.EtudesManager;

/**
 * Created by sarahlaforets on 14/01/15.
 */

public class SplashScreen extends AsyncTask<String, Void, Void> {
    public static final double MIN_TIME_SLPASHSCREEN = 3.0*1000;
    private Activity mActivity;

    public SplashScreen(Activity activity) {
        super();
        mActivity = activity;
    }

    @Override
    protected Void doInBackground(String... params) {
        long timeBefore = System.currentTimeMillis();
        // start parse services
        Parse.enableLocalDatastore(mActivity);
        Parse.initialize(mActivity, "lakYfYrJz40bbOusSjWNSauW7WLBUy833T4XioDq", "PQwqXZTw5klsFAbX0voD3yVxYpYvEfVwjVmL4Lnb");

        // download/upload if possible data from server
        EtudesManager.getInstance().getDatasFromParse(mActivity);
        long timeAfter = System.currentTimeMillis();

        if ((timeAfter - timeBefore) < MIN_TIME_SLPASHSCREEN) {
            try {
                Thread.sleep((long) (MIN_TIME_SLPASHSCREEN-(timeAfter - timeBefore)));
            } catch (Exception e) {
                Log.d("SplashScreen", "error");
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        Intent i = new Intent(mActivity, MainActivity.class);
        mActivity.startActivity(i);

        // close this activity
        mActivity.finish();
    }
}

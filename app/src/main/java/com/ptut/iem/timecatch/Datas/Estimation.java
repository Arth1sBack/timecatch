package com.ptut.iem.timecatch.Datas;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by iem on 07/01/15.
 */
public class Estimation implements Parcelable{
    long dureePresentee;
    long reponse;

    public Estimation (long dureePresentee, long reponse){
        this.dureePresentee = dureePresentee;
        this.reponse = reponse;
    }

    // nécessaire pour parcelable
    public int describeContents() {
        return 0;
    }
    // write your object's data to the passed-in Parcel
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(dureePresentee);
        out.writeLong(reponse);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Estimation> CREATOR = new Parcelable.Creator<Estimation>() {
        public Estimation createFromParcel(Parcel in) {
            return new Estimation(in);
        }

        public Estimation[] newArray(int size) {
            return new Estimation [size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Estimation(Parcel in) {
        dureePresentee = in.readLong();
        reponse = in.readLong();
    }

    public long getDureePresentee(){
        return dureePresentee;
    }

    public long getReponse() {
        return reponse;
    }
}

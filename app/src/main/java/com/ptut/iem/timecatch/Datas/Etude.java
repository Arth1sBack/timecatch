package com.ptut.iem.timecatch.Datas;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by iem on 07/01/15.
 */
public class Etude implements Parcelable{

    long duree_min;
    long duree_max;
    int n_rep;
    String id_etude;
    int nbNotif;
    Date dateDebut;
    Date dateFin;


    public Etude(long duree_min, long duree_max, int n_rep,String id_etude, int nbNotif, Date dateDebut, Date dateFin){
        this.duree_min = duree_min;
        this.duree_max = duree_max;
        this.n_rep = (n_rep<8)?8:n_rep;
        this.id_etude = id_etude;
        this.nbNotif = nbNotif;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    public int getN_rep(){
        return n_rep;
    }

    public long getDuree_min(){
        return duree_min;
    }

    public long getDuree_max(){
        return  duree_max;
    }

    // nécessaire pour parcelable
    public int describeContents() {
        return 0;
    }
    // write your object's data to the passed-in Parcel
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(duree_min);
        out.writeLong(duree_max);
        out.writeInt(n_rep);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Etude> CREATOR = new Parcelable.Creator<Etude>() {
        public Etude createFromParcel(Parcel in) {
            return new Etude(in);
        }

        public Etude[] newArray(int size) {
            return new Etude[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Etude(Parcel in) {
        duree_min = in.readLong();
        duree_max = in.readLong();
        n_rep = in.readInt();
    }

    public String getID(){
        return this.id_etude;
    }

    public Date getDateDebut(){
        return dateDebut;
    }

    public Date getDateFin(){
        return dateFin;
    }

    public int getNbNotif(){
        return nbNotif;
    }

}

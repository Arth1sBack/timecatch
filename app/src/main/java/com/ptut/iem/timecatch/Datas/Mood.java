package com.ptut.iem.timecatch.Datas;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by iem on 07/01/15.
 */
public class Mood implements Parcelable{
    private String badMood;
    private String goodMood;
    private int feeling;



    public Mood (String badMood, String goodMood){
        if (badMood == null){
            badMood = "";
        }
        if(goodMood == null){
            goodMood = "";
        }

        this.badMood = badMood;
        this.goodMood = goodMood;
        feeling = 3;
    }

    public Mood (String badMood, String goodMood, int feeling){
        this(badMood,goodMood);
        this.feeling = feeling;
    }



    public String getBadMood(){
        return badMood;
    }

    public String getGoodMood(){
        return goodMood;
    }

    public int getFeeling(){return feeling;}

    // nécessaire pour parcelable
    public int describeContents() {
        return 0;
    }
    // write your object's data to the passed-in Parcel
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(feeling);
        out.writeString(badMood);
        out.writeString(goodMood);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Mood> CREATOR = new Parcelable.Creator<Mood>() {
        public Mood createFromParcel(Parcel in) {
            return new Mood(in);
        }

        public Mood[] newArray(int size) {
            return new Mood[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Mood(Parcel in) {
        feeling = in.readInt();
        badMood = in.readString();
        goodMood = in.readString();
    }
}

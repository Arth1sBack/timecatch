package com.ptut.iem.timecatch.Datas;

import android.os.Parcelable;

import com.ptut.iem.timecatch.Singleton.EtudesManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by iem on 07/01/15.
 */
public class Reponse {

    String id_etude;
    String id_user;
    List<Mood> moods;
    Date date;
    List<Estimation>estimations;


    public Reponse(){
        moods = new ArrayList<>();
        estimations = new ArrayList<>();

    }

    public void setMoods(Parcelable tableOfMoods[]){
        for(Parcelable aMood : tableOfMoods){
            this.moods.add((Mood)aMood);
        }
    }

    public List<Mood> getMoods(){
        return moods;
    }

    public void setEstimations(Parcelable tableOfEstimation[]){
        for (Parcelable aEstimation : tableOfEstimation){
            this.estimations.add((Estimation)aEstimation);
        }
    }

    public List<Estimation> getEstimations(){
        return estimations;
    }

    public void setDate(){
        this.date = new Date();
    }

    public Date getDate(){
        return date;
    }

    public void setId_user(String id_user){
        this.id_user = id_user;
    }

    public String getId_user(){
        return id_user;
    }
}

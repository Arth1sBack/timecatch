package com.ptut.iem.timecatch.Notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by sarahlaforets on 15/01/15.
 */
public class CancelAlarmBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PendingIntent pendingIntent = intent.getParcelableExtra("key");
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(pendingIntent); //Annulation de la répétition de l'alarm manager lors de la fin de l'étude
        Log.d("cancelAlarm", "It stop");
    }
}

package com.ptut.iem.timecatch.Notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.ptut.iem.timecatch.Activity.MainActivity;
import com.ptut.iem.timecatch.R;

/**
 * Created by sarahlaforets on 15/01/15.
 */
public class CreateNotification extends BroadcastReceiver {
    int mNotifIndex;
    Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        Bundle mBundle;
        mBundle = intent.getExtras();
        mNotifIndex = mBundle.getInt("notifIndex", 0);
        Log.d("indexNotifCreateNotif", String.valueOf(mNotifIndex));
        //context.stopService(intent);
        createNotification();
    }

    private void createNotification() {
        // Prepare intent which is triggered if the
        // notification is selected
        int notificationIndex = mNotifIndex - 1;
        Intent mintent = new Intent(mContext, MainActivity.class);
        mintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //String sentValueIndex = String.valueOf(notificationIndex);
        mintent.putExtra("notifIndex", notificationIndex);
        Log.d("NotifEnvoyée", String.valueOf(notificationIndex));
        PendingIntent pIntent = PendingIntent.getActivity(mContext, notificationIndex, mintent, PendingIntent.FLAG_UPDATE_CURRENT);

        //TODO : Changer le text
        Notification notification = new Notification.Builder(mContext)
                .setContentTitle("Nouveau test")
                .setContentText("Un nouveau test vous attend").setSmallIcon(R.drawable.therapy)
                .setContentIntent(pIntent).build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
        // hide the notification after its selected
        notification.flags |= Notification.FLAG_ONGOING_EVENT;

        notificationManager.notify(0, notification);
    }
}

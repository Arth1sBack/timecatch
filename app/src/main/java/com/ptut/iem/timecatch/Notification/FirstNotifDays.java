package com.ptut.iem.timecatch.Notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by sarahlaforets on 15/01/15.
 */
public class FirstNotifDays {
    public Date dateFinEtude; // Date de fin de l'étude
    private Calendar calendar = Calendar.getInstance();
    int nbrNotif;  // nombre de notification par jour
    int debut = 0; // L'heure du lancement dans la journée pour la répétition
    PendingIntent mAlarmSender;
    AlarmManager am;
    Context mContext;

    public FirstNotifDays(Date dateFinEtude, int nbrNotif, Context mContext) {
        this.dateFinEtude = dateFinEtude;
        this.nbrNotif = nbrNotif;
        this.mContext = mContext;
    }

    public void ScheduledFirstNotifOfTheDay() {

        // récuperation des préferences utilisateurs
        SharedPreferences prefs = mContext.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        Date heureDeb = new Date(prefs.getLong("com.example.app.user_heureDeb",0));
        Date heureFin = new Date(prefs.getLong("com.example.app.user_heureFin",0));

        //Set alarm first notif Schedule
        // la class suivante va être appelé lors de l'alarme
        final Intent intent = new Intent(mContext, FirstNotificationReceiver.class);

        // Envoie de le nombre de notification lors de l'étude
        intent.putExtra("notifIndex", nbrNotif);

        mAlarmSender = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);

        // Planification de quand reveiller le service pour planifier la première notification
        // Ici ça sera le début de la journée
        calendar.set(Calendar.HOUR, heureDeb.getHours());
        calendar.set(Calendar.MINUTE, heureDeb.getMinutes() - 60);

        // On récupère la valeur en miliseconde
        long heureDebut = calendar.getTimeInMillis();
        Log.d("TimeBegin", String.valueOf(heureDebut));
        // Création de l'alarme avec une répétition tout les jours tant qu'on ne l'a pas arrêté
        am.setRepeating(AlarmManager.RTC_WAKEUP, heureDebut, AlarmManager.INTERVAL_DAY, mAlarmSender); // remplacer 2 * 60 * 1000 par AlarmManager.INTERVAL_DAY

        calendar.setTime(dateFinEtude); // Planification de la fin de l'étude

        // La class qui sera appeler stoppera l'alarm pour arrêter l'étude
        Intent cancellationIntent = new Intent(mContext, CancelAlarmBroadcastReceiver.class);
        cancellationIntent.putExtra("key", mAlarmSender); // envoie du pendingIntent pour pouvoir stopper l'alarm
        PendingIntent cancellationPendingIntent = PendingIntent.getBroadcast(mContext, 0, cancellationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long time = calendar.getTimeInMillis();
        Log.d("TimeEnd", String.valueOf(time));
        am.set(AlarmManager.RTC_WAKEUP, time, cancellationPendingIntent);
    }
}

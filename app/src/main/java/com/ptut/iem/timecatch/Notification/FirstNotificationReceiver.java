package com.ptut.iem.timecatch.Notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by sarahlaforets on 15/01/15.
 */
public class FirstNotificationReceiver extends BroadcastReceiver {
    private int mNotifIndex;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Bundle mBundle;
        mBundle = intent.getExtras();
        mNotifIndex = mBundle.getInt("notifIndex", 0);
        Intent nextIntent = new Intent(context, ScheduledNotificationService.class);
        nextIntent.putExtra("notifIndex", mNotifIndex);
        context.startService(nextIntent); // Lance le service
        Log.d("FirstNotifReceiver", "Pass ok");
    }
}

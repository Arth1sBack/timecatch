package com.ptut.iem.timecatch.Notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Created by sarahlaforets on 15/01/15.
 */
public class ScheduledNotificationService extends Service {
    private int mNotifIndex; // index des notifications
    private int nbrNotifLeft; // nombre de notification(s) restante(s)
    private Intent mIntent;

    public ScheduledNotificationService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        //Récupération des données envoyer par le receiver
        mIntent = intent;
        Bundle mBundle;
        if (mIntent != null) {
            mBundle = mIntent.getExtras();
            mNotifIndex = mBundle.getInt("notifIndex", 0); // Le nombre de notification en cours
            scheduleNotification();
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // code to execute when the service is first created
        super.onCreate();
        Log.i("MyService", "Service Started.");
    }

    // Planifier la notification
    public void scheduleNotification() {

        // Récupération du calendrier
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR); // Heure actuelle

        // récuperation des préferences utilisateurs
        SharedPreferences prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        Date heureDeb = new Date(prefs.getLong("com.example.app.user_heureDeb",0));
        Date heureFin = new Date(prefs.getLong("com.example.app.user_heureFin",0));

        int begin = (heureDeb.getHours() * 60) + (heureDeb.getMinutes()); // Paramètre debut de la plage de notification
        int end = (heureFin.getHours() * 60) + (heureDeb.getMinutes()); // Paramètre fin de la plage de notification

        Random rand = new Random();
        int random = 0;

        // Si on est dans la plage horaire
        // if (hour >= begin && hour < end) {

            // On set les paramètre à envoyer à la class qui va créer la notification
            Intent intent = new Intent(this, CreateNotification.class);
            intent.putExtra("notifIndex", mNotifIndex);
            PendingIntent mAlarmSender = PendingIntent.getBroadcast(this, mNotifIndex, intent, 0);
            nbrNotifLeft = mNotifIndex - 1;

            Log.d("end", String.valueOf(end));
            Log.d("hour", String.valueOf(hour));
            Log.d("indexNotif", String.valueOf(mNotifIndex));

            // durée qui va s'écouler à partir du moment présent jusqu'à la prohchaine notification
            //random = rand.nextInt((end - nbrNotifLeft * 60) - hour);
            int min, max;
            min = hour + 60;
            max = end - (nbrNotifLeft * 60);
            random = rand.nextInt((max - min) + 1) + min;
            int value = random - min;
            Log.d("Random", String.valueOf(value));
            c.add(Calendar.MINUTE, value);
            long firstTime = c.getTimeInMillis();

            // Schedule the alarm!
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.set(AlarmManager.RTC_WAKEUP, firstTime, mAlarmSender);
        //}

    }

    @Override
    public void onDestroy() {
        Log.d("MyService", "Service stop");
    }
}

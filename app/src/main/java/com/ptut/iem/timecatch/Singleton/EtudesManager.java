package com.ptut.iem.timecatch.Singleton;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.ptut.iem.timecatch.Datas.Estimation;
import com.ptut.iem.timecatch.Datas.Etude;
import com.ptut.iem.timecatch.Datas.Mood;
import com.ptut.iem.timecatch.Datas.Reponse;
import com.ptut.iem.timecatch.Notification.FirstNotifDays;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by iem on 07/01/15.
 */
public class EtudesManager {

    private final String etude_id = "com.example.app.etude_id";
    private final String etude_duree_min = "com.example.app.etude_duree_min";
    private final String etude_duree_max = "com.example.app.etude_duree_max";
    private final String etude_n_rep = "com.example.app.etude_n_rep";
    private final String etude_nb_notif = "com.example.app.etude_nb_notif";
    private final String etude_date_debut = "com.example.app.etude_date_debut";
    private final String etude_date_fin = "com.example.app.etude_date_fin";


    private static EtudesManager   _instance;

    private Etude etude, dataFromParse;
    private List moods;
    private Reponse lastReponse;
    private List<String> moodsId;

    private Context context;
    private SharedPreferences prefs;

    private FirstNotifDays firstNotifDays;

    private EtudesManager()
    {
        lastReponse = new Reponse();
        moods = new ArrayList();
        loadEtude();
    }

    public synchronized static EtudesManager getInstance()
    {
        if (_instance == null)
        {
            _instance = new EtudesManager();
        }
        return _instance;
    }

    public List<Mood> getMoods(){
        return this.moods;
    }

    public Reponse getLastReponse(){
        return lastReponse;
    }

    public void cleanLastReponse(){
        lastReponse = new Reponse();
    }

    // récupère la derniere étude et set les mood
    public void getDatasFromParse(Context context){
        this.context = context;

        prefs = context.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Study");

        // Retrieve the most recent ones
        query.orderByDescending("createdAt");

        // Only retrieve the last one
        query.setLimit(1);

        // Include the post data with each comment
        query.include("MoodPairs");


        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject study, ParseException e) {
                if (study == null) {
                    Log.d("score", "The getFirst request failed.");
                } else {
                    Log.d("score", "Retrieved the object.");
                    setEtude(study);
                    setMoods(study);
                }
            }
        });
    }

    public void setEtude(ParseObject retrivedStudy){
        String id_etude = retrivedStudy.getObjectId();
        int n_rep = retrivedStudy.getInt("n_rep");
        long duree_1 = retrivedStudy.getLong("duree_1");
        long duree_2 = retrivedStudy.getLong("duree_2");
        int nbNotif = retrivedStudy.getInt("nbNotif");
        Date dateDeb = retrivedStudy.getDate("date_debut");
        Date dateFin = retrivedStudy.getDate("date_fin");

        dataFromParse = new Etude(duree_1,duree_2,n_rep,id_etude, nbNotif,dateDeb,dateFin);

        //si aucune étude en cours, on prend les données de parse
        if(etude.getID()==null){
            etude = dataFromParse;
            saveStudyLocally(etude);
        }

        //si l'etude dl est la meme que celle en mémoire
        if (dataFromParse.getID() == etude.getID()){
            //rien ne se passe
        }else{
            if(etude.getDateFin().after(new Date())){
                //on continue de bosser avec l'étude gardée en local
            }else{
                // on change d'étude de travail
                etude = dataFromParse;
                saveStudyLocally(etude);

                // appel de la première notif
                firstNotifDays = new FirstNotifDays(dateFin, nbNotif, context);
                firstNotifDays.ScheduledFirstNotifOfTheDay();
            }
        }
    }

    public void setMoods(ParseObject retrievedStudy){
        moodsId = retrievedStudy.getList("MoodPairs");
        for (Object aMood : moodsId){
            getMoodsName(aMood.toString());
        }
    }

    private void getMoodsName(String retrievedMood){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("MoodPairs");
        query.getInBackground(retrievedMood, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    // object will be study
                    getNames(object);
                } else {
                    // something went wrong
                }
            }
        });
    }

    // Récupère les noms des moods et en fait des instances de l'objet
    private void getNames(ParseObject object){
        String mood1;
        String mood2;

        // Différencier les languages
        String lang = Locale.getDefault().getLanguage().toString();
        if(lang.equals("fr")){
            mood1 = object.getString("moodNegative_fr");
            mood2 = object.getString("moodPositive_fr");
        }else{
            mood1 = object.getString("moodNegative_en");
            mood2 = object.getString("moodPositive_en");
        }

        Mood aMood = new Mood(mood1,mood2);
        moods.add(aMood);
    }

    public Etude getEtude(){
        return etude;
    }


    public void sendThisShit(Reponse reponse){
        ParseObject answer = new ParseObject("Answers");
        answer.put("idUser",reponse.getId_user());
        answer.put("dateTimeMoods", reponse.getDate());

        for(String aMoodId : moodsId){
            answer.add("moodsId",aMoodId);
        }
        for(Mood aMood : reponse.getMoods()){
            answer.add("moodsValue",aMood.getFeeling());
        }
        for(Estimation aEstimation : reponse.getEstimations()){
            answer.add("displayedDuration",aEstimation.getDureePresentee());
            answer.add("estimatedDuration",aEstimation.getReponse());
        }

        answer.saveEventually();

        cleanLastReponse();
    }

    public void saveStudyLocally(Etude etude){
        prefs.edit().putString(etude_id, etude.getID()).apply();
        prefs.edit().putLong(etude_duree_max, etude.getDuree_max()).apply();
        prefs.edit().putLong(etude_duree_min, etude.getDuree_min()).apply();
        prefs.edit().putInt(etude_n_rep,etude.getN_rep()).apply();
        prefs.edit().putInt(etude_nb_notif,etude.getNbNotif()).apply();
        prefs.edit().putLong(etude_date_debut,etude.getDateDebut().getTime()).apply();
        prefs.edit().putLong(etude_date_fin,etude.getDateFin().getTime()).apply();
    }

    public void loadEtude(){
        try{
            //recharge l'étude depuis les prefs
            String id_etude = prefs.getString(etude_id,"training");
            Long dureeMax = prefs.getLong(etude_duree_max, 2200);
            Long dureeMin = prefs.getLong(etude_duree_min,1000);
            int n_rep = prefs.getInt(etude_n_rep,8);
            int nbNotif = prefs.getInt(etude_nb_notif,3);
            Date dateDebut = new Date(prefs.getLong(etude_date_debut,0));
            Date dateFin = new Date(prefs.getLong(etude_date_fin,0));

            this.etude = new Etude(dureeMin,dureeMax,n_rep,id_etude,nbNotif,dateDebut,dateFin);

        }catch (NullPointerException e){
            //aucune etude sauvegardée
            etude = new Etude(1000,2200,24,"default_99",0,new Date(),new Date());
        }

    }
}

